package com.wyl.shiro.business.service;

import com.wyl.shiro.business.entity.UserRole;
import com.wyl.shiro.framework.object.AbstractService;

/**
 * 用户角色
 *
 * @date 2021/5/18 15:16
 */
public interface SysUserRoleService extends AbstractService<UserRole, Long> {

    /**
     * 添加用户角色
     *
     * @param userId
     * @param roleIds
     */
    void addUserRole(Long userId, String roleIds);

    /**
     * 根据用户ID删除用户角色
     *
     * @param userId
     */
    void removeByUserId(Long userId);
}
