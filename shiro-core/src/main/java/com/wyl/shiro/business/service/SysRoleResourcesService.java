package com.wyl.shiro.business.service;

import com.wyl.shiro.business.entity.RoleResources;
import com.wyl.shiro.framework.object.AbstractService;

/**
 * 角色资源
 *
 * @date 2021/5/18 15:16
 */
public interface SysRoleResourcesService extends AbstractService<RoleResources, Long> {

    /**
     * 添加角色资源
     *
     * @param roleId
     * @param resourcesIds
     */
    void addRoleResources(Long roleId, String resourcesIds);

    /**
     * 通过角色id批量删除
     *
     * @param roleId
     */
    void removeByRoleId(Long roleId);
}
