package com.wyl.shiro.business.vo;

import com.wyl.shiro.business.entity.Resources;
import com.wyl.shiro.framework.object.BaseConditionVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ResourceConditionVO extends BaseConditionVO {
    private String type;
}

