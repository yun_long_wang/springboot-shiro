package com.wyl.shiro.business.vo;

import com.wyl.shiro.business.entity.User;
import com.wyl.shiro.framework.object.BaseConditionVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class UserConditionVO extends BaseConditionVO {
    private User user;
}
