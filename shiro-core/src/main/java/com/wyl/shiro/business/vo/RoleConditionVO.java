package com.wyl.shiro.business.vo;

import com.wyl.shiro.business.entity.Role;
import com.wyl.shiro.framework.object.BaseConditionVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class RoleConditionVO extends BaseConditionVO {
    private Role role;
}

