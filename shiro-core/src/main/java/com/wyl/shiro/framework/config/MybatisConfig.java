package com.wyl.shiro.framework.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.stereotype.Component;

@Component
@MapperScan("com.wyl.shiro.persistence.mapper")
public class MybatisConfig {
}
