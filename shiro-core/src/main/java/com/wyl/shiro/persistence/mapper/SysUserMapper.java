package com.wyl.shiro.persistence.mapper;

import com.wyl.shiro.business.vo.UserConditionVO;
import com.wyl.shiro.persistence.beans.SysUser;
import com.wyl.shiro.plugin.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysUserMapper extends BaseMapper<SysUser> {

    List<SysUser> findPageBreakByCondition(UserConditionVO vo);

    List<SysUser> listByRoleId(Long roleId);

}
