package com.wyl.shiro.persistence.mapper;

import com.wyl.shiro.persistence.beans.SysRoleResources;
import com.wyl.shiro.plugin.BaseMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface SysRoleResourcesMapper extends BaseMapper<SysRoleResources> {
}
