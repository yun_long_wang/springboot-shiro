package com.wyl.shiro.persistence.mapper;

import com.wyl.shiro.persistence.beans.SysUserRole;
import com.wyl.shiro.plugin.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
    List<Integer> findUserIdByRoleId(Integer roleId);
}
